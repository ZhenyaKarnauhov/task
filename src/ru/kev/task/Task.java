package ru.kev.task;


import java.util.Arrays;
import java.util.Collections;

/**
 * Класс, реализация программы "разминка".
 *
 * @author Karnauhov Evgeniy 15ИТ18
 */
public class Task {
    public static void main(String[] args) {

        int time = 4;
        Integer[] arrayOFOrders = {1, 2, 3, 4, 5};

        if (CheckTime(time,arrayOFOrders)){
            Arrays.sort(arrayOFOrders, Collections.reverseOrder());
            System.out.println("Время = " + time + "\nМаксимальная зарплата, " +
                    "которую можно получить на заданное время = " + maxMoney(time, arrayOFOrders));
        } else{
            System.out.println("time - неверное значение");
        }


    }

    /**
     * Метод для проверки правильного значения time.
     *
     * @param time время, для выполнения заказов
     * @param orders массив с заказами
     * @return true, если time > 0 && time<=orders.length, иначе - false.
     */
    private static boolean CheckTime(int time, Integer[] orders) {
        return time > 0 && time <= orders.length;
    }

    /**
     * Метод для нахождения максимальной заработанной суммы денег,
     * которую можно получить в пределах доступного времени за выполнение
     * фирмой самых ценных заказов из предложенных.
     *
     * @param time            время, которое есть у фирмы.
     * @param orderValue массив,содержит стоимости заказов.
     * @return максимальная заработанная сумма денег, которую можно получить в пределах доступного времени.
     */
    private static int maxMoney(int time, Integer[] orderValue) {

        int result = 0;

        for (int i = 0; i < time; i++) {
            result += orderValue[i];
        }

        return result;
    }

}